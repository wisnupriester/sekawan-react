import { useState } from 'react'
import './App.css'
import { initReactI18next } from 'react-i18next'
import Header from './Header'
import Sidebar from './Sidebar'
import Home from './Home'
import i18nextBrowserLanguagedetector from 'i18next-browser-languagedetector'


function App() {
  const [openSidebarToggle, setOpenSidebarToggle] = useState(false)

  const OpenSidebar = () => {
    setOpenSidebarToggle(!openSidebarToggle)
  }

  return (
    <div className='grid-container'>
      <Header OpenSidebar={OpenSidebar}/>
      <Sidebar openSidebarToggle={openSidebarToggle} OpenSidebar={OpenSidebar}/>
      <Home />
    </div>
  )
}

export default App