import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import { LoginSignup } from './LoginSignup.jsx'
LoginSignup

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
