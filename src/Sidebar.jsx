import React from 'react'
import 
{BsWind, BsGrid1X2Fill, BsPeopleFill, BsPerson, BsBookmarkCheckFill,
   BsMenuButtonWideFill, BsFillGearFill,BsTicketPerforated, BsPinMapFill}
 from 'react-icons/bs'

function Sidebar({openSidebarToggle, OpenSidebar}) {
  return (
    <aside id="sidebar" className={openSidebarToggle ? "sidebar-responsive": ""}>
        <div className='sidebar-title'>
            <div className='sidebar-brand'>
                <BsWind  className='icon_header'/> NDASBOARD
            </div>
            <span className='icon close_icon' onClick={OpenSidebar}>X</span>
        </div>

        <ul className='sidebar-list'>
            <li className='sidebar-list-item'>
                <a href="">
                    <BsGrid1X2Fill className='icon'/> Overview
                </a>
            </li>
            <li className='sidebar-list-item'>
                <a href="">
                    <BsTicketPerforated className='icon'/> Tickets
                </a>
            </li>
            <li className='sidebar-list-item'>
                <a href="">
                    <BsTicketPerforated className='icon'/> Ideas
                </a>
            </li>
            <li className='sidebar-list-item'>
                <a href="">
                    <BsPeopleFill className='icon'/> Contact
                </a>
            </li>
            <li className='sidebar-list-item'>
                <a href="">
                    <BsPerson className='icon'/> Customers
                </a>
            </li>
            <li className='sidebar-list-item'>
                <a href="">
                    <BsPinMapFill className='icon'/> Agents
                </a>
            </li>
            <li className='sidebar-list-item'>
                <a href="">
                    <BsMenuButtonWideFill className='icon'/> Article
                </a>
            </li>
            <li className='sidebar-separator'></li>
            <li className='sidebar-list-item'>
                <a href="">
                    <BsFillGearFill className='icon'/> Setting
                </a>
            </li>
            <li className='sidebar-list-item'>
                <a href="">
                    <BsBookmarkCheckFill className='icon'/> Subscribtions
                </a>
            </li>
        </ul>
    </aside>
  )
}

export default Sidebar