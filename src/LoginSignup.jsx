import React, { useState } from 'react';
import './App.css';
import { BsWind, BsGrid1X2Fill, BsPeopleFill, BsPerson, BsBookmarkCheckFill, BsMenuButtonWideFill, BsFillGearFill, BsTicketPerforated, BsPinMapFill, BsPersonFill, BsEnvelopeAtFill, BsFileLock2Fill } from 'react-icons/bs';

export const LoginSignup = () => {
  const [action, setAction] = useState("Login");

  return (
    <div className='container'>
      <div className='login-header'>
        <div className='text'>
          <div className='text'>{action}</div>
          <div className='underline'></div>
        </div>
        <div className='inputs'>
          {action === "Login" ? <div></div> : <div className='input'>
            <BsPersonFill className='icon' />
            <input type="text" placeholder='Name' />
          </div>}
          <div className='input'>
            <BsEnvelopeAtFill className='icon' />
            <input type="email" placeholder='Email' />
          </div>
          <div className='input'>
            <BsFileLock2Fill className='icon' />
            <input type="password" placeholder='Password' />
          </div>
        </div>
        <div className="submit">Submit</div>
      </div>
      {action === "Sign Up" ? <div></div> : <div className='forgot-password'>Forgot Password? <span>Click Here!</span></div>}
      <div className='submit-container'>        
        <div className={action === "Login" ? "submit gray" : "submit"} onClick={() => { setAction("Sign Up") }}>Sign Up</div>
        <div className={action === "Sign Up" ? "submit gray" : "submit"} onClick={() => { setAction("Login") }}>Login</div>
      </div>
    </div>
  );
};
